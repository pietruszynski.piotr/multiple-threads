package pl.edu.pw.ee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pl.edu.pw.ee.threads.MultithreadedServer;

import static java.util.Objects.requireNonNull;

@SpringBootApplication
public class ServerApplication implements CommandLineRunner {

    private final MultithreadedServer multithreadedServer;

    @Autowired
    public ServerApplication(final MultithreadedServer multithreadedServer) {
        this.multithreadedServer = requireNonNull(multithreadedServer, "multithreaded server");
    }

    public static void main(String[] args) {
        SpringApplication.run(ServerApplication.class, args);
    }

    @Override
    public void run(String... args) throws InterruptedException {
        startListening();
    }

    private void startListening() throws InterruptedException {
        new Thread(multithreadedServer).start();
        Thread.sleep(10000);
        multithreadedServer.stop();
    }

}
