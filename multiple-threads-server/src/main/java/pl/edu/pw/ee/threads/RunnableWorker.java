package pl.edu.pw.ee.threads;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

import static java.util.Objects.requireNonNull;

public class RunnableWorker implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(RunnableWorker.class);

    private final Socket clientSocket;

    private final String serverText;

    public RunnableWorker(final Socket clientSocket,
                          final String serverText) {
        this.clientSocket = requireNonNull(clientSocket, "client socket");
        this.serverText = requireNonNull(serverText, "server text");
    }

    @Override
    public void run() {
        writeToClient();
        printMessages();
        closeSocket();
    }

    private void writeToClient() {
        try (OutputStream outputStream = clientSocket.getOutputStream()) {
            long time = System.currentTimeMillis();
            outputStream.write(("Client: " + serverText + "-" + time).getBytes());
        } catch (IOException e) {
            logger.error("Error while using outputStream: " + e);
        }
    }

    private void printMessages() {
        boolean isInterrupted = false;
        while (!isInterrupted) {
            try {
                logger.info("Running thread");
                Thread.sleep(20 * 100);
            } catch (InterruptedException e) {
                isInterrupted = true;
                logger.info("Thread was interrupted");
            }
        }
    }

    private void closeSocket() {
        try {
            clientSocket.close();
            logger.info("Successful closing client socket");
        } catch (IOException e) {
            logger.info("Socket wasn't closed");
        }
    }
}
