package pl.edu.pw.ee.threads;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.requireNonNull;

@Component
public class MultithreadedServer implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(MultithreadedServer.class);

    private final List<Thread> threads = new ArrayList<>();

    private final int port;

    private ServerSocket server;

    private boolean isStopped = false;

    @Autowired
    public MultithreadedServer(@Value("${multithreaded-server.port}") final Integer port) {
        this.port = requireNonNull(port, "port");
    }

    @Override
    public void run() {
        try {
            this.server = new ServerSocket(this.port);
            acceptClient(this.server);
        } catch (IOException e) {
            throw new RuntimeException("Error while opening server socket: ", e);
        }
    }

    public void stop() {
        this.isStopped = true;
        try {
            this.server.close();
        } catch (IOException e) {
            throw new RuntimeException("Error while opening server socket: ", e);
        }
        threads.forEach(Thread::interrupt);
    }

    private void acceptClient(ServerSocket server) {
        while (!isStopped()) {
            try {
                Socket clientSocket = server.accept();
                logger.info("New client connected.");
                Thread thread = new Thread(new RunnableWorker(clientSocket, "Multithreaded server"));
                threads.add(thread);
                thread.start();
            } catch (IOException e) {
                if (isStopped()) {
                    logger.info("Server was stopped");
                    return;
                }
                logger.error("Error while opening client socket: " + e);
            }
        }
    }

    private boolean isStopped() {
        return this.isStopped;
    }

}
