package pl.edu.pw.ee.threads;

import lombok.extern.slf4j.Slf4j;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.concurrent.TimeUnit;

import static java.util.Objects.requireNonNull;

@Slf4j
public class RunnableWorker implements Runnable {

    private final Socket clientSocket;

    private final String serverText;

    public RunnableWorker(final Socket clientSocket,
                          final String serverText) {
        this.clientSocket = requireNonNull(clientSocket, "client socket");
        this.serverText = requireNonNull(serverText, "server text");
    }

    @Override
    public void run() {
        writeToClient();
        printMessages();
        closeSocket();
    }

    private void writeToClient() {
        try (OutputStream outputStream = clientSocket.getOutputStream()) {
            long time = System.currentTimeMillis();
            outputStream.write(("Client: " + serverText + "-" + time).getBytes());
        } catch (IOException e) {
            log.error("Error while using outputStream: " + e);
        }
    }

    private void printMessages() {
        boolean isInterrupted = false;
        while (!isInterrupted) {
            try {
                log.info("Running thread");
                TimeUnit.MILLISECONDS.sleep(2000);
            } catch (InterruptedException e) {
                isInterrupted = true;
                log.info("Thread was interrupted");
            }
        }
    }

    private void closeSocket() {
        try {
            clientSocket.close();
            log.info("Successful closing client socket");
        } catch (IOException e) {
            log.info("Socket wasn't closed");
        }
    }
}
