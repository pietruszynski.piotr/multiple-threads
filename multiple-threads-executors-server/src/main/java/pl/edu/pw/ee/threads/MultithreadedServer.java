package pl.edu.pw.ee.threads;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.util.Objects.requireNonNull;

@Slf4j
@Component
public class MultithreadedServer implements Runnable {

    private final ExecutorService executorServices = Executors.newCachedThreadPool();

    private final int port;

    private ServerSocket server;

    private boolean isStopped = false;

    @Autowired
    public MultithreadedServer(@Value("${multithreaded-server.port}") final Integer port) {
        this.port = requireNonNull(port, "port");
    }

    @Override
    public void run() {
        try {
            this.server = new ServerSocket(this.port);
            acceptClient(this.server);
        } catch (IOException e) {
            throw new RuntimeException("Error while opening server socket: ", e);
        }
    }

    public void stop() {
        this.isStopped = true;
        try {
            this.server.close();
        } catch (IOException e) {
            throw new RuntimeException("Error while opening server socket: ", e);
        }
        executorServices.shutdownNow();
    }

    private void acceptClient(ServerSocket server) {
        while (!isStopped()) {
            try {
                Socket clientSocket = server.accept();
                log.info("New client connected.");
                executorServices.submit(new RunnableWorker(clientSocket, "Multithreaded server"));
            } catch (IOException e) {
                if (isStopped()) {
                    log.info("Server was stopped");
                    return;
                }
                log.error("Error while opening client socket: " + e);
            }
        }
    }

    private boolean isStopped() {
        return this.isStopped;
    }

}
