package pl.edu.pw.ee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pl.edu.pw.ee.threads.MultithreadedServer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static java.util.Objects.requireNonNull;

@SpringBootApplication
public class ServerApplication implements CommandLineRunner {

    private final MultithreadedServer multithreadedServer;

    private ExecutorService executorService = Executors.newSingleThreadExecutor();

    @Autowired
    public ServerApplication(final MultithreadedServer multithreadedServer) {
        this.multithreadedServer = requireNonNull(multithreadedServer, "multithreaded server");
    }

    public static void main(String[] args) {
        SpringApplication.run(ServerApplication.class, args);
    }

    @Override
    public void run(String... args) throws InterruptedException {
        startListening();
    }

    private void startListening() throws InterruptedException {
        executorService.submit(multithreadedServer);
        TimeUnit.MILLISECONDS.sleep(30000);
        multithreadedServer.stop();
    }

}
