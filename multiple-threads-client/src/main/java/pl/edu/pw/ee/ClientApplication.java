package pl.edu.pw.ee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pl.edu.pw.ee.threads.Client;

@SpringBootApplication
public class ClientApplication implements CommandLineRunner {

    private final Client client;

    @Autowired
    public ClientApplication(Client client) {
        this.client = client;
    }

    public static void main(String[] args) {
        SpringApplication.run(ClientApplication.class, args);
    }

    @Override
    public void run(String... args) {
        client.start();
    }

}
