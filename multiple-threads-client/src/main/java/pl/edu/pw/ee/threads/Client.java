package pl.edu.pw.ee.threads;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

import static java.util.Objects.requireNonNull;

@Component
public class Client {

    private static final Logger logger = LoggerFactory.getLogger(Client.class);

    private final String host;

    private final int port;

    @Autowired
    public Client(@Value("${multithreaded-server.host}") String host,
                  @Value("${multithreaded-server.port}") int port) {
        this.host = requireNonNull(host, "host");
        this.port = requireNonNull(port, "port");
    }

    public void start() {
        for (int i = 0; i < 100; i++) {
            connect();
        }
    }

    private void connect() {
        try (Socket socket = new Socket(this.host, this.port)) {
            InputStream inputStream = socket.getInputStream();
            byte[] bytes = inputStream.readAllBytes();
            logger.info(new String(bytes, StandardCharsets.UTF_8));
        } catch (IOException e) {
            logger.info("Exception while read bytes: ", e);
        }
    }

}
